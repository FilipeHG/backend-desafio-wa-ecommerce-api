# Backend - DDD REST API
API modelo do projeto de e-commerce para challenge da WA.

## Getting Started
- CTRL+B | F5

** Tests/Dev => Use this JWT Bearer Token:

"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IndhLWVjb21tZXJjZS1zeXN0ZW0iLCJuYmYiOjE2NjU0NTA5NDIsImV4cCI6MTk4MTA3MDEzNywiaWF0IjoxNjY1NDUwOTQyfQ.OjzEOBfMCYw8bRC9wLbeSTwNroykPNesOH1edSCdp-c"

## Build and Test
F5

### Contribute
FILIPE GONÇALVES - filipeh.goncalves@gmail.com
